cmake_minimum_required(VERSION 3.15)
project(AhmetNedimProgramming1Project)

set(CMAKE_CXX_STANDARD 17)

add_executable(AhmetNedimProgramming1Project main.cpp)